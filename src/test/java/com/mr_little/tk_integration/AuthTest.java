package com.mr_little.tk_integration;

import com.mr_little.tk_integration.api.models.User;
import com.mr_little.tk_integration.internal.interfaces.IBaseDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthTest {
    private final JacksonJsonParser jsonParser = new JacksonJsonParser();

    @Autowired
    private MockMvc mvc;

    @MockBean
    IBaseDao<User, Long> userDao;

    @MockBean
    User user;

    @Test
    public void shouldPassSignUp() throws Exception {
        User newUser = new User("User", "user@mail.ru", "123456");

        MvcResult result = this.mvc.perform(post("/api/auth/signup")
            .content("{\"username\":\""+newUser.getUsername()+"\",\"email\":\""+newUser.getEmail()+"\", \"password\": \""+newUser.getPassword()+"\"}")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();

        Map<String, Object> resultMap = jsonParser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals("User registered successfully!", resultMap.get("message"));
    }

    @Test
    public void shouldNotPassPasswordValidationSignUp() throws Exception {
        MvcResult result = this.mvc.perform(post("/api/auth/signup")
            .content("{\"username\":\"User\",\"email\":\"user@mail.ru\", \"password\": \"12345\"}")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andReturn();

        Map<String, Object> resultMap = jsonParser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals("Password should be between 6 and 40 characters long", resultMap.get("message"));
        Assert.assertEquals("error", resultMap.get("status"));
    }

    @Test
    public void shouldNotPassEmailValidationSignUp() throws Exception {
        MvcResult result = this.mvc.perform(post("/api/auth/signup")
            .content("{\"username\":\"User\",\"email\":\"user?mail.ru\", \"password\": \"123456\"}")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andReturn();

        Map<String, Object> resultMap = jsonParser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals("must be a well-formed email address", resultMap.get("message"));
        Assert.assertEquals("error", resultMap.get("status"));
    }

    @Test
    public void shouldPassSingIn() throws Exception {
        User newUser = new User("User", "user@gmail.com", "$2a$10$yEiJ3EXx2IMTBALNpRCUBO9W9Ab9g4GDql2ybcCPzye9RcuyTnHRi");
        when(userDao.getByField(any(), any())).thenReturn(Optional.of(newUser));

        MvcResult result = this.mvc.perform(post("/api/auth/signin")
            .content("{\"email\":\""+newUser.getEmail()+"\", \"password\": \"123456\"}")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();

        Map<String, Object> resultMap = jsonParser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals(newUser.getUsername(), resultMap.get("username"));
        Assert.assertEquals(newUser.getEmail(), resultMap.get("email"));
    }

    @Test
    public void shouldNotPassSingIn() throws Exception {
        MvcResult result = this.mvc.perform(post("/api/auth/signin")
            .content("{\"email\":\"user@gmail.com\", \"password\": \"123456\"}")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isUnauthorized())
            .andReturn();

        Map<String, Object> resultMap = jsonParser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals("Authentication failed: [Bad credentials]", resultMap.get("message"));
        Assert.assertEquals("error", resultMap.get("status"));
    }
}
