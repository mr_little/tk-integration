package com.mr_little.tk_integration;

import com.mr_little.tk_integration.api.models.CVApplication;
import com.mr_little.tk_integration.api.models.Profile;
import com.mr_little.tk_integration.api.models.User;
import com.mr_little.tk_integration.internal.services.CVApplicationService;
import com.mr_little.tk_integration.internal.services.CustomUserDetails;
import com.mr_little.tk_integration.internal.services.FileUploadService;
import com.mr_little.tk_integration.internal.exceptions.FileUploadException;
import com.mr_little.tk_integration.internal.interfaces.IBaseDao;
import com.mr_little.tk_integration.security.jwt.JwtUtils;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FileUploadTest {

    private final JacksonJsonParser jsonParser = new JacksonJsonParser();

    @Autowired
    private MockMvc mvc;

    @Autowired
    JwtUtils jwtUtils;

    @MockBean
    FileUploadService fileUploadService;

    @MockBean
    IBaseDao<User, Long> userDao;

    @MockBean
    IBaseDao<CVApplication, Long> cvApplicationDao;

    @MockBean
    Authentication authentication;

    @Test
    public void shouldUploadFile() throws Exception, FileUploadException {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.doc",
                "text/plain", "Spring Framework".getBytes());

        User user = new User("User", "user@gmail.com", "$2a$10$yEiJ3EXx2IMTBALNpRCUBO9W9Ab9g4GDql2ybcCPzye9RcuyTnHRi");
        CVApplication cv = new CVApplication("", user);
        cv.setId(1L);

        when(userDao.getByField(any(), any())).thenReturn(Optional.of(user));
        when(authentication.getPrincipal()).thenReturn(new CustomUserDetails(1L, user.getUsername(), user.getEmail(), user.getPassword()));
        when(cvApplicationDao.getByField(any(), any())).thenReturn(Optional.of(cv));
        when(fileUploadService.uploadFile(any(), any())).thenReturn("superFileName");
        CVApplicationService cvApplicationService = mock(CVApplicationService.class);
        when(cvApplicationService.save(any(), any())).thenReturn(cv);

        String token = jwtUtils.generateJwtToken(authentication);

        MvcResult result = this.mvc.perform(multipart("/api/user/cv/submit").file(multipartFile)
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andReturn();

        Map<String, Object> resultMap = jsonParser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals(1, resultMap.get("fileId"));
        Assert.assertEquals("PROGRESS", resultMap.get("status"));
    }

    @Test
    public void shouldNotUploadFile() throws Exception, FileUploadException {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.doc",
                "text/plain", "Spring Framework".getBytes());

        User user = new User("User", "user@gmail.com", "$2a$10$yEiJ3EXx2IMTBALNpRCUBO9W9Ab9g4GDql2ybcCPzye9RcuyTnHRi");
        CVApplication cv = new CVApplication("", user);
        cv.setId(1L);

        when(userDao.getByField(any(), any())).thenReturn(Optional.of(user));
        when(authentication.getPrincipal()).thenReturn(new CustomUserDetails(1L, user.getUsername(), user.getEmail(), "$2a$10$yEiJ3EXx2IMTBALNpRCUBO9W9Ab9g4GDql2ybcCPzye9RcuyTnHRi"));
        when(cvApplicationDao.getByField(any(), any())).thenReturn(Optional.of(cv));

        String token = jwtUtils.generateJwtToken(authentication);

        MvcResult result = this.mvc.perform(multipart("/api/user/cv/submit").file(multipartFile)
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isBadRequest())
                .andReturn();

        Assert.assertEquals("Something went wrong and we could not save your cv application", result.getResponse().getContentAsString());
    }

    @Test
    public void shouldRetrieveInformation() throws Exception, FileUploadException {
        User user = new User("User", "user@gmail.com", "$2a$10$yEiJ3EXx2IMTBALNpRCUBO9W9Ab9g4GDql2ybcCPzye9RcuyTnHRi");
        CVApplication cv = new CVApplication("", user);
        cv.setId(1L);
        cv.setStatus("OK");
        cv.setProfile(new Profile("Petra", "van de Ven", "Hoofdstraat", "15-A", "6717 AA", "EDE"));

        when(userDao.getByField(any(), any())).thenReturn(Optional.of(user));
        when(authentication.getPrincipal()).thenReturn(new CustomUserDetails(1L, user.getUsername(), user.getEmail(), user.getPassword()));
        when(cvApplicationDao.get(1L)).thenReturn(Optional.of(cv));

        String token = jwtUtils.generateJwtToken(authentication);

        MvcResult result = this.mvc.perform(get("/api/user/cv/retrieve/{processId}", 1L)
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andReturn();

        JSONObject profileObject = new JSONObject(result.getResponse().getContentAsString()).getJSONObject("profile");

        Map<String, Object> resultMap = jsonParser.parseMap(result.getResponse().getContentAsString());
        Map<String, Object> addressMap = jsonParser.parseMap(profileObject.getString("address"));

        Assert.assertEquals("OK", resultMap.get("profileCheckStatus"));
        Assert.assertEquals("Petra", profileObject.getString("firstName"));
        Assert.assertEquals("van de Ven", profileObject.getString("lastName"));
        Assert.assertEquals("Hoofdstraat", addressMap.get("streetName"));
        Assert.assertEquals("15-A", addressMap.get("streetNumberBase"));
        Assert.assertEquals("6717 AA", addressMap.get("postalCode"));
        Assert.assertEquals("EDE", addressMap.get("city"));
    }

    @Test
    public void shouldNotRetrieveInformation() throws Exception, FileUploadException {
        User user = new User("User", "user@gmail.com", "$2a$10$yEiJ3EXx2IMTBALNpRCUBO9W9Ab9g4GDql2ybcCPzye9RcuyTnHRi");
        CVApplication cv = new CVApplication("", user);
        cv.setId(1L);

        when(userDao.getByField(any(), any())).thenReturn(Optional.of(user));
        when(authentication.getPrincipal()).thenReturn(new CustomUserDetails(1L, user.getUsername(), user.getEmail(), user.getPassword()));

        String token = jwtUtils.generateJwtToken(authentication);

        MvcResult result = this.mvc.perform(get("/api/user/cv/retrieve/{processId}", 2L)
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isBadRequest())
                .andReturn();

        Map<String, Object> resultMap = jsonParser.parseMap(result.getResponse().getContentAsString());
        Assert.assertEquals("CV with ID: '2' not exists", resultMap.get("message"));
        Assert.assertEquals("error", resultMap.get("status"));
    }
}
