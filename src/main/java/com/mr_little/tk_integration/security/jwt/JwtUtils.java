package com.mr_little.tk_integration.security.jwt;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.mr_little.tk_integration.internal.services.CustomUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import com.auth0.jwt.JWT;

import java.util.Date;

@Component
public class JwtUtils {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationMs}")
    private int jwtExpirationMs;

    public String generateJwtToken(Authentication authentication) {

        CustomUserDetails userPrincipal = (CustomUserDetails) authentication.getPrincipal();
        return JWT.create()
                .withSubject(userPrincipal.getEmail())
                .withExpiresAt(new Date((new Date()).getTime() + jwtExpirationMs))
                .withIssuedAt(new Date())
                .sign(Algorithm.HMAC256(jwtSecret.getBytes()));
    }

    public String getUserNameFromJwtToken(String authToken) {
        DecodedJWT jwt = validateJwtToken(authToken);
        if (jwt != null) {
            return jwt.getSubject();
        }
        return null;
    }

    public DecodedJWT validateJwtToken(String authToken) {
        Algorithm algorithm = Algorithm.HMAC256(jwtSecret.getBytes());
        JWTVerifier verifier = JWT.require(algorithm).build();
        try {
            return verifier.verify(authToken);
        } catch (JWTVerificationException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        }
        return null;
    }
}
