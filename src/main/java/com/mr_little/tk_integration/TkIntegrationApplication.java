package com.mr_little.tk_integration;

import com.mr_little.tk_integration.internal.interfaces.IFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


/**
* The TK integration spring boot application that
 * simply provide interface to upload CV application, asynchronously validate it in the TK sourcebox,
 * and present the result via GET request
 * @author Boris Rostovskiy
 * @since 23-07-2020
* */
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class TkIntegrationApplication implements CommandLineRunner {

	@Autowired
	IFileService storageService;

	public static void main(String[] args) {
		SpringApplication.run(TkIntegrationApplication.class, args);
	}

	@Override
	public void run(String... arg) throws Exception {
		storageService.deleteAll();
	}
}
