package com.mr_little.tk_integration.internal.interfaces;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface IBaseDao<T, ID> {
    Optional<T> get(ID id);
    List<T> getAll();
    void save(T t);
    void delete(T t);
    void update(T t);

    <F> boolean existsByField(String field, F value);
    <F> Optional<T> getByField(String field, F value);
}
