package com.mr_little.tk_integration.internal.interfaces;

import com.mr_little.tk_integration.api.models.User;
import com.mr_little.tk_integration.internal.exceptions.FileUploadException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface IFileService {
    String uploadFile(MultipartFile file, User user) throws FileUploadException;
    void createUserFilesFolder(User user) throws IOException;
    void deleteAll();
}
