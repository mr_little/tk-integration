package com.mr_little.tk_integration.internal.interfaces;

public interface PrimaryKeyGenerator<ID> {
    /**
     * Gives the next primary key given the previous maximum primary key value.
     *
     * @return the next primary key
     */
    ID nextPrimaryKey();
}
