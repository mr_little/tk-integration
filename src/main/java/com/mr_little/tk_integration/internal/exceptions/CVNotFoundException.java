package com.mr_little.tk_integration.internal.exceptions;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class CVNotFoundException extends UsernameNotFoundException {
    public CVNotFoundException(String msg) {
        super(msg);
    }
}
