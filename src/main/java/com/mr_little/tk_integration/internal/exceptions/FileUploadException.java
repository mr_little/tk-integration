package com.mr_little.tk_integration.internal.exceptions;

public class FileUploadException extends Throwable {
    public FileUploadException(String message) {
        super(message);
    }

    public FileUploadException(String message, Throwable cause) {
        super(message, cause);
    }
}
