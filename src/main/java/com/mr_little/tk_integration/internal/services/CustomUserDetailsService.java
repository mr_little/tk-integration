package com.mr_little.tk_integration.internal.services;

import com.mr_little.tk_integration.api.models.User;
import com.mr_little.tk_integration.internal.interfaces.IBaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    IBaseDao<User, Long> userDao;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userDao.getByField("email", email).orElseThrow(() -> new UsernameNotFoundException("User '"+email+"' not found"));
        return CustomUserDetails.build(user);
    }
}
