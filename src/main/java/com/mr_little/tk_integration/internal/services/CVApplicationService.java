package com.mr_little.tk_integration.internal.services;

import com.mr_little.tk_integration.api.models.CVApplication;
import com.mr_little.tk_integration.api.models.Profile;
import com.mr_little.tk_integration.api.models.User;
import com.mr_little.tk_integration.internal.exceptions.CVNotFoundException;
import com.mr_little.tk_integration.internal.interfaces.IBaseDao;

import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Optional;


@Service
public class CVApplicationService {
    private static final Logger logger = LoggerFactory.getLogger(CVApplicationService.class);
    private static final HttpClient httpClient =  HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build();

    @Autowired
    IBaseDao<CVApplication, Long> cvApplicationDao;

    @Value("${app.upload.url}")
    private String url;

    @Value("${app.upload.account}")
    private String account;

    @Value("${app.upload.username}")
    private String username;

    @Value("${app.upload.password}")
    private String password;

    public CVApplication save(String pathToFile, User user) throws NullPointerException {
        if (pathToFile == null) {
            logger.error("Path to file should not been empty");
            return null;
        }
        File file = new File(pathToFile);

        Optional<CVApplication> cvApplicationOptional = cvApplicationDao.getByField("fileName", file.getName());
        if (cvApplicationOptional.isPresent()) {
            return cvApplicationOptional.get();
        }

        CVApplication c = new CVApplication(file.getName(), user);
        cvApplicationDao.save(c);
        checkApplicationInExtractorServiceAsync(c, file);
        return c;
    }

    public String getStatus(Long id, User user) throws CVNotFoundException {
        CVApplication cv = cvApplicationDao.get(id)
                .orElseThrow(() -> new CVNotFoundException("CV with ID: '"+id+"' not exists"));

        if (!cv.getAddedBy().equals(user)) {
            logger.error("User '"+user.getEmail()+"' attempts to get status of the cv '"+cv.getId()+"'");
            throw new CVNotFoundException("CV with ID: '"+id+"' not exists");
        }
        return cv.getStatus();
    }

    public Profile getProfile(Long id, User user) throws CVNotFoundException {
        CVApplication cv = cvApplicationDao.get(id)
                .orElseThrow(() -> new CVNotFoundException("CV with ID: '"+id+"' not exists"));

        if (!cv.getAddedBy().equals(user)) {
            logger.error("User '"+user.getEmail()+"' attempts to get status of the cv '"+cv.getId()+"'");
            throw new CVNotFoundException("CV with ID: '"+id+"' not exists");
        }

        return cv.getProfile();
    }

    @Async("threadPoolTaskExecutor")
    public void checkApplicationInExtractorServiceAsync(CVApplication application, File file) {
            FileBody uploadFilePart = new FileBody(file);
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("uploaded_file", uploadFilePart);
            builder.addTextBody("account", account);
            builder.addTextBody("username", username);
            builder.addTextBody("password", password);
            HttpPost post = new HttpPost(url);
            post.setEntity(builder.build());

            HttpResponse response;
            try {
                response = httpClient.execute(post);
            } catch (IOException e) {
                logger.error("Http client catch an error:", e);
                application.setStatus("ERROR");
                application.setLastUpdate(new Date());
                cvApplicationDao.update(application);
                return;
            }

            if (response.getStatusLine().getStatusCode() != 200) {
                logger.error("TK extractor respond with non 200 status:["+response.getStatusLine().getStatusCode()+"]: "+response.getStatusLine());
                application.setStatus("ERROR");
                application.setLastUpdate(new Date());
                cvApplicationDao.update(application);
                return;
            }

            String xmlResponse;
            try {
                xmlResponse = getContent(response);
            } catch (IOException e) {
                logger.error("Cannot parse response:", e);
                application.setStatus("ERROR");
                application.setLastUpdate(new Date());
                cvApplicationDao.update(application);
                return;
            }

            try {
                Document documentResponse = loadXMLFromString(xmlResponse);
                NodeList personData = documentResponse.getElementsByTagName("Profile");
                if (personData.getLength() == 0 || personData.getLength() > 1) {
                    throw new ParserConfigurationException("Xml response has too much information:["+xmlResponse+"]");
                }
                Element eElement = (Element) personData.item(0);
                Element addressElement = (Element) eElement.getElementsByTagName("Address").item(0);

                String firstName = eElement.getElementsByTagName("FirstName").item(0).getTextContent();
                String lastName = eElement.getElementsByTagName("LastName").item(0).getTextContent();
                String StreetName = addressElement.getElementsByTagName("StreetName").item(0).getTextContent();
                String StreetNumberBase = addressElement.getElementsByTagName("StreetNumberBase").item(0).getTextContent();
                String PostalCode = addressElement.getElementsByTagName("PostalCode").item(0).getTextContent();
                String City = addressElement.getElementsByTagName("City").item(0).getTextContent();

                application.setStatus("OK");
                application.setLastUpdate(new Date());
                application.setProfile(new Profile(firstName, lastName, StreetName, StreetNumberBase, PostalCode, City));
                cvApplicationDao.update(application);

            } catch (ParserConfigurationException | IOException | SAXException e) {
                logger.error("Cannot load xml from response:", e);
                application.setStatus("ERROR");
                application.setLastUpdate(new Date());
                cvApplicationDao.update(application);
            }
    }

    private static Document loadXMLFromString(String xmlString) throws ParserConfigurationException, IOException, SAXException {
        final InputStream stream = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        return builder.parse(stream);
    }

    private static String getContent(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String body = "";
        StringBuilder content = new StringBuilder();

        while ((body = rd.readLine()) != null) {
            content.append(body).append("\n");
        }
        return content.toString().trim();
    }
}
