package com.mr_little.tk_integration.internal.services;

import com.mr_little.tk_integration.api.models.User;
import com.mr_little.tk_integration.internal.exceptions.FileUploadException;
import com.mr_little.tk_integration.internal.interfaces.IFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

@Service
public class FileUploadService implements IFileService {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadService.class);

    @Value("${app.upload.dir}")
    public String uploadDir;

    private final String staticFolder = Paths.get("resources","static").toString();

    @Override
    public String uploadFile(MultipartFile file, User user) throws FileUploadException {

        Path copyLocation = Paths
                .get(Paths.get(staticFolder, uploadDir, user.getUsername(),
                        file.getOriginalFilename()).toString());

        try {
            File prevVersionFile = new File(copyLocation.toString());
            if (prevVersionFile.exists()) {
                byte[] fileContent = Files.readAllBytes(prevVersionFile.toPath());

                Checksum checksumEngine1 = new CRC32();
                checksumEngine1.update(fileContent, 0, file.getBytes().length);
                long checksum1 = checksumEngine1.getValue();

                Checksum checksumEngine2 = new CRC32();
                checksumEngine2.update(file.getBytes(), 0, file.getBytes().length);
                long checksum2 = checksumEngine1.getValue();

                logger.info("File does exist, let's compare checksum's:" + checksum1 + " "+checksum2 );
                // Update only if files are not equal
                if (checksum1 != checksum2) {
                    logger.info("File: "+file.getOriginalFilename()+" was updated.");
                    Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
                }
                checksumEngine1.reset();
                checksumEngine2.reset();
            } else {
                logger.info("File not exists, let's create one!:" + copyLocation );
                Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
            }


        } catch (Exception e) {
            logger.error("error while upload file", e);
            throw new FileUploadException("Could not store file " + file.getOriginalFilename() + ". Please try again!");
        }

        return copyLocation.toString();
    }

    @Override
    public void createUserFilesFolder(User user) throws IOException {
        File userFolder = new File(Paths.get(staticFolder, uploadDir, user.getUsername()).toString());
        Path source = Paths.get(this.getClass().getResource("/").getPath());
        logger.info("Create user folder: "+userFolder+" SOURCE "+source.toString());
        if (!userFolder.exists()) {
            userFolder.mkdirs();
        }
    }

    @Override
    public void deleteAll() {
        Path root = Paths.get(staticFolder, uploadDir);
        FileSystemUtils.deleteRecursively(root.toFile());
    }
}
