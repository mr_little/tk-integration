package com.mr_little.tk_integration.api.payload.response;

import com.mr_little.tk_integration.api.models.Profile;

public class ProfileResponse {
    private String profileCheckStatus;
    private Profile profile;

    public ProfileResponse(String profileCheckStatus, Profile profile) {
        this.profileCheckStatus = profileCheckStatus;
        this.profile = profile;
    }
}
