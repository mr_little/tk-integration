package com.mr_little.tk_integration.api.payload.response;

public class ErrorResponse{
    private String errorMsg;
    private String status;


    public ErrorResponse(String msg) {
        this.errorMsg = msg;
        this.status = "ERROR";
    }

    public ErrorResponse(String msg, String err) {
        this.errorMsg = msg;
        this.status = err;
    }

    public String getStatus() {
        return status;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
