package com.mr_little.tk_integration.api.payload.response;

public class UserCreatedResponse {
    private Long userId;
    private String message;

    public UserCreatedResponse(Long userId) {
        this.userId = userId;
        this.message = null;
    }
    public UserCreatedResponse(Long userId, String message) {
        this.userId = userId;
        this.message = message;
    }

    public Long getUserId() {
        return userId;
    }

    public String getMessage() {
        return message;
    }
}
