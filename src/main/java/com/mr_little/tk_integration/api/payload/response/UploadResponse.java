package com.mr_little.tk_integration.api.payload.response;

public class UploadResponse {
    private Long fileId;
    private String status;

    public UploadResponse(Long fileId, String status) {
        this.fileId = fileId;
        this.status = status;
    }

    public Long getFileId() {
        return fileId;
    }

    public String getStatus() {
        return status;
    }
}
