package com.mr_little.tk_integration.api.controllers;

import javax.validation.Valid;

import com.mr_little.tk_integration.api.models.User;
import com.mr_little.tk_integration.api.payload.request.LoginRequest;
import com.mr_little.tk_integration.api.payload.request.SignupRequest;
import com.mr_little.tk_integration.api.payload.response.ErrorResponse;
import com.mr_little.tk_integration.api.payload.response.JwtResponse;
import com.mr_little.tk_integration.api.payload.response.UserCreatedResponse;
import com.mr_little.tk_integration.internal.services.CustomUserDetails;
import com.mr_little.tk_integration.internal.interfaces.IBaseDao;
import com.mr_little.tk_integration.security.jwt.JwtUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.authentication.AuthenticationManager;

import java.util.Set;


/**
 * Auth controller
 * Perform authorization and authentication processes on api.
 * @author Boris Rostovskiy
 * @since 23-07-2020
 * */
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    IBaseDao<User, Long> userDao;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;


    /**
     * Perform authorization user in the application. Return authorization token
     * */

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail()
        ));
    }

    /**
     * Perform registration user in the application
    * */
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userDao.existsByField("username", signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new ErrorResponse("Username '"+signUpRequest.getUsername()+"' is already taken!"));
        }

        if (userDao.existsByField("email", signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new ErrorResponse("Email '"+signUpRequest.getEmail()+"' is already in use!"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));
        Set<String> strRoles = signUpRequest.getRole();
//        Set<Role> roles = new HashSet<>();

//        if (strRoles == null) {
//            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
//                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//            roles.add(userRole);
//        } else {
//            strRoles.forEach(role -> {
//                switch (role) {
//                    case "admin":
//                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(adminRole);
//
//                        break;
//                    case "mod":
//                        Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(modRole);
//
//                        break;
//                    default:
//                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(userRole);
//                }
//            });
//        }

//        user.setRoles(roles);
        userDao.save(user);
        return ResponseEntity.ok(new UserCreatedResponse(user.getId(), "User registered successfully!"));
    }
}
