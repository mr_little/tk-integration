package com.mr_little.tk_integration.api.controllers;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mr_little.tk_integration.api.models.CVApplication;
import com.mr_little.tk_integration.api.models.Profile;
import com.mr_little.tk_integration.api.models.User;
import com.mr_little.tk_integration.api.payload.response.ProfileResponse;
import com.mr_little.tk_integration.api.payload.response.UploadResponse;
import com.mr_little.tk_integration.internal.services.CVApplicationService;
import com.mr_little.tk_integration.internal.services.CustomUserDetails;
import com.mr_little.tk_integration.internal.exceptions.FileUploadException;
import com.mr_little.tk_integration.internal.interfaces.IBaseDao;
import com.mr_little.tk_integration.internal.interfaces.IFileService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@RestController
@RequestMapping("/api/user/cv")
public class CVFormController {

    private static final Logger logger = LoggerFactory.getLogger(CVFormController.class);

    @Autowired
    IFileService fileService;

    @Autowired
    CVApplicationService cvApplicationService;

    @Autowired
    IBaseDao<User, Long> userDao;

    @PostMapping("/submit")
    public ResponseEntity<?> submit(@RequestParam("file") MultipartFile file, Authentication authentication)
            throws FileUploadException {

        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        String email = userDetails.getEmail();

        User user = userDao.getByField("email", email)
                .orElseThrow(() -> new UsernameNotFoundException("Unable to locate user with name: " + email));
        try {
            fileService.createUserFilesFolder(user);
        } catch (IOException e) {
            logger.error("CreateUserFilesFolder", e);
            throw new FileUploadException("Cannot upload file because cannot create user files folder");
        }

        String pathToFile = fileService.uploadFile(file, user);
        CVApplication cv = cvApplicationService.save(pathToFile, user);

        if (cv == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Something went wrong and we could not save your cv application");
        }

        return ResponseEntity.ok(new UploadResponse(cv.getId(), cv.getStatus()));
    }

    @GetMapping("/retrieve/{processId}")
    public ResponseEntity<?> retrieve(
                                      @PathVariable("processId") Long processId,
                                      Authentication authentication) throws JsonProcessingException {
        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        String email = userDetails.getEmail();
        User user = userDao.getByField("email", email)
                .orElseThrow(() -> new UsernameNotFoundException("Unable to locate user with name: " + email));
        String status = cvApplicationService.getStatus(processId, user);
        Profile profile = cvApplicationService.getProfile(processId, user);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        ProfileResponse profileResponse = new ProfileResponse(status, profile);
        String jsonProfile = objectMapper.writeValueAsString(profileResponse);

        return ResponseEntity.ok(jsonProfile);
    }
}
