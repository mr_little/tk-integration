package com.mr_little.tk_integration.api.controllers;

import com.mr_little.tk_integration.api.payload.response.MessageResponse;
import com.mr_little.tk_integration.internal.exceptions.CVNotFoundException;
import com.mr_little.tk_integration.internal.exceptions.FileUploadException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * App Exception Handler controller
 * Intercept all EXPECTABLE errors in the application
 * @author Boris Rostovskiy
 * @since 23-07-2020
 * */
@ControllerAdvice
public class AppExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(AppExceptionHandler.class);

    @ExceptionHandler(FileUploadException.class)
    public ModelAndView handleException(FileUploadException exception, RedirectAttributes redirectAttributes) {
        logger.trace("FileUploadException", exception);
        ModelAndView mav = new ModelAndView();
        mav.addObject("message", exception.getMessage());
        mav.setViewName("error");
        return mav;
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ModelAndView handleException(HttpMessageNotReadableException exception, RedirectAttributes redirectAttributes) {
        logger.trace("HttpMessageNotReadableException", exception);
        ModelAndView mav = new ModelAndView();
        mav.addObject("message", exception.getMessage());
        mav.setViewName("error");
        return mav;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        logger.trace("MethodArgumentNotValidException", exception);
        FieldError error = exception.getBindingResult().getFieldErrors().get(0);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new MessageResponse(error.getDefaultMessage(), "error"));
    }

    @ExceptionHandler(CVNotFoundException.class)
    public ResponseEntity<?> handleCVNotFoundException(CVNotFoundException exception) {
        logger.trace("CVNotFoundException", exception);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new MessageResponse(exception.getMessage(), "error")
        );
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<?> handleAuthenticationException(AuthenticationException exception) {
        logger.trace("AuthenticationException", exception);
        return ResponseEntity.status((HttpStatus.UNAUTHORIZED)).body(new MessageResponse("Authentication failed: ["+exception.getMessage()+"]", "error"));
    }
}
