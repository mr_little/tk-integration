package com.mr_little.tk_integration.api.models;

import java.util.Date;

public class CVApplication extends AHasID<Long> {
    private Long id;
    private Date addDate;
    private Date lastUpdate;
    private String fileName;
    private String status;
    private User addedBy;
    private Profile profile;

    public CVApplication() {}

    public CVApplication(String fileName, User user) {
        this.fileName = fileName;
        this.addedBy = user;
        this.addDate = new Date();
        this.status = "PROGRESS";
        this.profile = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getAddDate() {
        return addDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    public void setLastUpdate() {
        this.lastUpdate = new Date();
    }

    public User getAddedBy() {
        return addedBy;
    }

    public String getFileName() {
        return fileName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
