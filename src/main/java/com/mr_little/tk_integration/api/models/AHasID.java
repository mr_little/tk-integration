package com.mr_little.tk_integration.api.models;

import java.io.Serializable;

public class AHasID<ID extends Serializable & Comparable<ID>> {
    private ID id;
    public ID getId() {
        return id;
    };
    public void setId(ID id){
        this.id = id;
    };
}
