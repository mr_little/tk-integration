package com.mr_little.tk_integration.api.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

public class Profile {
    private String firstName;
    private String lastName;
    private class Address {
        private String streetName;
        private String streetNumberBase;
        private String postalCode;
        private String city;
        public Address(String streetName, String streetNumberBase, String postalCode, String city) {
            this.streetName = streetName;
            this.streetNumberBase = streetNumberBase;
            this.postalCode = postalCode;
            this.city = city;
        }

        @Override
        public String toString() {
            return "{" +
                    "streetName='" + streetName + '\'' +
                    ", streetNumberBase='" + streetNumberBase + '\'' +
                    ", postalCode='" + postalCode + '\'' +
                    ", city='" + city + '\'' +
                    '}';
        }
    }
    private Address address;

    public Profile(String firstName, String lastName, String streetName, String streetNumberBase, String postalCode, String city) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = new Address(streetName, streetNumberBase, postalCode, city);
    }

    public Address getAddress() {
        return address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address=" + address +
                '}';
    }
}
