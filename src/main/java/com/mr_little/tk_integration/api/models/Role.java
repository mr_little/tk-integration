package com.mr_little.tk_integration.api.models;

public class Role {
    private ERole name;

    public ERole getName() {
        return name;
    }

    public void setName(ERole name) {
        this.name = name;
    }
}
