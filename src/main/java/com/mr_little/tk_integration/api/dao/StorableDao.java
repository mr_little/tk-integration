package com.mr_little.tk_integration.api.dao;

import com.mr_little.tk_integration.api.models.AHasID;
import com.mr_little.tk_integration.internal.interfaces.IBaseDao;
import com.mr_little.tk_integration.internal.interfaces.PrimaryKeyGenerator;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public abstract class StorableDao<T extends AHasID<ID>, ID extends Serializable & Comparable<ID>> implements IBaseDao<T, ID> {
    protected final Map<ID, T> entities = new ConcurrentHashMap<>();
    protected PrimaryKeyGenerator<ID> primaryKeyGenerator;

    @Override
    public Optional<T> get(ID id) {
        return Optional.ofNullable(entities.get(id));
    }

    @Override
    public List<T> getAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void save(T item) {
        if (item == null) {
            throw new IllegalArgumentException("CV entity must not be null");
        }
        ID id = item.getId();
        if (id == null) {
            if (primaryKeyGenerator != null) {
                id = primaryKeyGenerator.nextPrimaryKey();
                item.setId(id);
            } else {
                throw new IllegalStateException("CV primary key is null: " + item);
            }
        }
        entities.put(id, item);
    }

    @Override
    public void delete(T item) {
        get(item.getId()).ifPresent(theItem -> {
            entities.remove(theItem.getId());
        });
    }

    @Override
    public void update(T item) {
        get(item.getId()).ifPresent(theItem -> {
            entities.put(item.getId(), item);
        });
    }

    public <F> boolean existsByField(String fieldName, F value) {
        return findByField(fieldName, value).size() > 0;
    }
    private <F> List<T> findByField(String fieldName, F value) {
        if (fieldName == null) {
            throw new IllegalArgumentException("fieldName must not be null");
        }
        List<T> foundEntities = new ArrayList<>();
        for (T entity : entities.values()) {
            Field field;
            try {
                field = getField(entity, fieldName);
                AccessController.doPrivileged(new PrivilegedAction<Void>() {
                    @Override
                    public Void run() {
                        field.setAccessible(true);
                        return null;
                    }
                });
                if (haveSameValue(value, field.get(entity))) {
                    foundEntities.add(entity);
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new IllegalArgumentException("Field not found or not accessible: " + fieldName);
            }
        }
        return foundEntities;
    }

    private static boolean haveSameValue(Object o1, Object o2) {
        if (o1 == null) {
            return o2 == null;
        } else {
            return o1.equals(o2);
        }
    }

    private Field getField(Object entity, String fieldName) throws NoSuchFieldException {
        Class<?> entityType = entity.getClass();
        while (entityType != null) {
            for (Field field : entityType.getDeclaredFields()) {
                if (field.getName().equals(fieldName)) {
                    return field;
                }
            }
            entityType = entityType.getSuperclass();
        }
        throw new NoSuchFieldException(fieldName);
    }

    public <F> Optional<T> getByField(String fieldName, F value) {
        List<T> l = findByField(fieldName, value);
        if (l.size() == 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(l.get(0));
    }
}
