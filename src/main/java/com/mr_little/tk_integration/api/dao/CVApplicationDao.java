package com.mr_little.tk_integration.api.dao;

import com.mr_little.tk_integration.api.models.CVApplication;
import com.mr_little.tk_integration.internal.interfaces.PrimaryKeyGenerator;
import org.springframework.stereotype.Component;

import java.lang.Long;

@Component
public class CVApplicationDao extends StorableDao<CVApplication, Long> {
    public CVApplicationDao() {
        this.primaryKeyGenerator = new LongPrimaryGenerator();
    }
}

