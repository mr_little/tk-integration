package com.mr_little.tk_integration.api.dao;

import com.mr_little.tk_integration.api.models.User;
import com.mr_little.tk_integration.internal.interfaces.PrimaryKeyGenerator;

import org.springframework.stereotype.Component;

@Component
public class UserDao extends StorableDao<User, Long> {
    public UserDao() {
        this.primaryKeyGenerator = new LongPrimaryGenerator();
    }
}
