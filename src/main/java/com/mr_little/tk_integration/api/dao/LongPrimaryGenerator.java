package com.mr_little.tk_integration.api.dao;

import com.mr_little.tk_integration.internal.interfaces.PrimaryKeyGenerator;

import java.util.concurrent.atomic.AtomicLong;

public class LongPrimaryGenerator implements PrimaryKeyGenerator<Long> {
    AtomicLong maxId = new AtomicLong();

    @Override
    public Long nextPrimaryKey() {
        return maxId.incrementAndGet();
    }
}
