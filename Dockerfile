FROM openjdk:8-jdk-alpine
#RUN addgroup -S spring && adduser -S spring -G spring
#USER spring:spring

WORKDIR /opt/tk_integration
COPY . /opt/tk_integration
RUN ./gradlew build

ENTRYPOINT ["java","-jar","./build/libs/tk_integration-0.0.1-SNAPSHOT.jar"]