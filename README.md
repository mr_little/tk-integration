# The TK Extraction service
The TK Extraction service can be used to extract information from CVs and enrich them with
e.g. synonyms. It takes as input authentication tokens and a binary file, and it returns a XML
document containing the extracted and enriched information. The API documentation is
attached as appendix below.

### Create a protected asynchronous proxy for the TK Extraction service using Java, with functionality:
 * `/submit?access_token=<ACCESS_TOKEN>` Accept a binary file for processing and return a processid
 * `/retrieve/<PROCESSID>?access_token=<ACCESS_TOKEN> (GET)` Return processing status or processing result
 * Retrieve an access token

## Build and run
```
docker-compose up --build
```

## Simple usage
1. Update all environment variables in docker-compose, if needs.
2. Build and run the application with `docker-compose up --build`
3. Browse the api service documentation `curl -X GET http://localhost:PORT/swagger-ui.html`
4. Make call `curl -X POST -H "Content-Type: application/json" -d '{"username":"User","email":"user@gmail.com", "password": "123456"}' http://localhost:PORT/api/auth/signup` to register new user in the system
5. Make call `curl -X POST -H "Content-Type: application/json" -d '{"email":"user@gmail.com", "password": "123456"}' http://localhost:PORT/api/auth/signin` to sign user in and obtain new application token 
6. Use obtained token to perform call `curl -X POST -H "Authorization: Bearer APPLICATION_TOKEN" -F 'file=@PATH_TO_FILE_TO_UPLOAD' http://localhost:PORT/api/user/cv/submit` to upload file to system
7. Use obtained token to perform call `curl -X GET -H "Authorization: Bearer APPLICATION_TOKEN" 'http://localhost:PORT/api/user/cv/retrieve/PROCESS_ID'` to retrieve information about CV
